#include "pch.h"
#include "ISorter.h"
#include <string>

class BubbleSort : public ISorter
{
public:
	BubbleSort() { }
	~BubbleSort() { }
	int* sort(int* arrPtr, int lenght) {

		for (int i = 0; i < lenght - 1; i++)
			for (int k = 0; k < lenght - 1; k++)
				if (arrPtr[k] > arrPtr[k+1])
					swap(arrPtr + k, arrPtr + k + 1);

		return arrPtr;
	}
	std::string get_name() { return "Bubble Sort"; }
};