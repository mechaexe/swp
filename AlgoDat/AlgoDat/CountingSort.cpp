#include "pch.h"
#include "ISorter.h"
#include <string>
#include <iostream>

class CountingSort : public ISorter
{
public:
	CountingSort() { }
	~CountingSort() { }
	int* sort(int* arrPtr, int lenght) {

		//std::cout << "WHYYY";
		int max = arrPtr[0];
		int min = arrPtr[0];
		int i = 1;

		for (; i < lenght; i++) {
			if (max < arrPtr[i])
				max = arrPtr[i];
			if (min > arrPtr[i])
				min = arrPtr[i];
		}

		int range = max - min + 1;
		int* count = new int[range] {0};
		int* output = new int[lenght] {0};
		
		for (i = 0; i < lenght; i++)
			count[arrPtr[i] - min]++;

		for (i = 1; i < range; i++) 
			count[i] += count[i - 1];

		for (i = lenght - 1; i >= 0; i--)
		{
			output[count[arrPtr[i] - min] - 1] = arrPtr[i];
			count[arrPtr[i] - min]--;
		}

		for (i = 0; i < lenght; i++)
			arrPtr[i] = output[i];
		
		delete[] count;
		count = nullptr;
		delete[] output;
		output = nullptr;
		return arrPtr;
	}

	std::string get_name() { return "Counting Sort"; }
};