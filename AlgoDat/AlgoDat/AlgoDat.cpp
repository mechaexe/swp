// AlgoDat.cpp : Diese Datei enthält die Funktion "main". Hier beginnt und endet die Ausführung des Programms.
//

#include "pch.h"
#include "ISorter.h"
#include "BubbleSort.cpp"
#include "SelectionSort.cpp"
#include "CountingSort.cpp"
#include <iostream>
#include <cstdio>
#include <ctime>

void run_testcase(int initlenght);
void print_array(int* arrPtr, int len);
int* generate_array(int size);
bool test_array(int* arrPtr, int lenght);
void test_algorithm(int lenght, ISorter* sorter);

int main()
{
	const int lenght = 100000000;
	run_testcase(lenght);
}

void run_testcase(int initlenght) {
	const int runs = 1;
	SelectionSort ss;
	BubbleSort bs;
	CountingSort cs;
	for (int i = 1; i <= runs; i++) {
		auto lenght = i * initlenght;
		test_algorithm(lenght, &cs);
		test_algorithm(lenght, &ss);
		//test_algorithm(lenght, &bs);
	}
}

void test_algorithm(int lenght, ISorter* sorter) {
	auto arrPtr = generate_array(lenght);
	auto start = clock();

	arrPtr = sorter->sort(arrPtr, lenght);

	auto duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;
	auto status = test_array(arrPtr, lenght) ? "Success" : "Failed";

	//print_array(arrPtr, lenght);

	std::cout << sorter->get_name() << "\t " << lenght << " Elements\t" << duration << "s\t" << status << std::endl;
	delete[] arrPtr;
	arrPtr = nullptr;
}

void print_array(int* arrPtr, int len) {
	for (int i = 0; i < len; i++)
		std::cout << "Address 0x" << (arrPtr + i) << ", value " << arrPtr[i] << std::endl;
}

bool test_array(int* arrPtr, int lenght) {
	int test = *arrPtr;
	for (int i = 1; i < lenght; i++) {
		if (*(arrPtr + i) < test)
			return false;

		test = *(arrPtr + i);
	}
	return true;
}

int* generate_array(int size) {
	int* arr = new int[size];

	for (int i = 0; i < size; i++)
		arr[i] = rand();

	return arr;
}