#include "pch.h"
#include "ISorter.h"
#include <string>

class SelectionSort : public ISorter
{
public:
	SelectionSort() { }
	~SelectionSort() { }
	int* sort(int* arrPtr, int lenght)
	{
		int* min = nullptr;

		for (int i = 0; i < lenght; i++) {
			min = arrPtr + i;

			for (int k = i + 1; k < lenght; k++) 
				if (*min > arrPtr[k])
					min = arrPtr + k;

			swap(arrPtr + i, min);
		}
		return arrPtr;
	}
	std::string get_name() { return "Selection Sort"; }
};