#pragma once

#include <string>

#ifndef I_Sorter_H
#define I_Sorter_H

class ISorter
{
public:
	virtual int* sort(int* arrPtr, int lenght) = 0;
	virtual std::string get_name() = 0;

protected:
	void swap(int* a, int* b) {
		int s = *a;
		*a = *b;
		*b = s;
	}
};
#endif // !I_Sorter_H
