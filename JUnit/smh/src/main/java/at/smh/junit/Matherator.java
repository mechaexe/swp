package at.smh.junit;

public class Matherator implements Adder, Subtractor{

    public long add(long... operands){
        long res = 0;
        for(long o : operands)
            res += o;
        return res;
    }

    public long subtract(long... operands){
        long res = operands[0];
        for(int i = 1; i<operands.length; i++)
            res -= operands[i];
        return res;
    }
}