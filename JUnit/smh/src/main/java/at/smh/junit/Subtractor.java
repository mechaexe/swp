package at.smh.junit;

public interface Subtractor{
    long subtract(long... operands);
}