package at.smh.junit;

public interface Adder{
    long add(long... operands);
}