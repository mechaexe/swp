package at.smh.junit;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MatheratorTest{
    private Matherator _Matherator;

    @Before
    public void setUp() throws Exception{
        _Matherator = new Matherator();
    }

    @Test
    public void testSubtract(){
        long result = 10 - 23 - (-1) - 16;
        assertEquals(result, _Matherator.subtract(10, 23, -1, 16));

        result = 100 - 102;
        assertEquals(result, _Matherator.subtract(100, 102));

        result = 0;
        assertEquals(result, _Matherator.subtract(0));
    }

    @Test
    public void testAdd(){
        long result = 10 + 23 + (-1) + 16;
        assertEquals(result, _Matherator.add(10, 23, -1, 16));

        result = 100 + 102;
        assertEquals(result, _Matherator.add(100, 102));

        result = 0;
        assertEquals(result, _Matherator.add(0));
    }
}