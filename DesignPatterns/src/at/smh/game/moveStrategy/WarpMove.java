package at.smh.game.moveStrategy;

import at.smh.game.PointF;
import org.newdawn.slick.GameContainer;

import java.awt.geom.Dimension2D;

public class WarpMove implements IMoveable {
    private IMoveable subMover;
    private Dimension2D dimensions;

    public WarpMove(Dimension2D dims){
        subMover = new MoveDown(new PointF(50, -(float)dims.getHeight()));
        this.dimensions = dims;
    }

    @Override
    public void update(GameContainer gameContainer, int i) {
        PointF loc = getLocation();

        if(loc.getY() > gameContainer.getHeight() + dimensions.getHeight())
            subMover = new MoveDown(new PointF(50, -(float)dimensions.getHeight()));

        subMover.update(gameContainer, i);
    }

    @Override
    public PointF getLocation() {
        return subMover.getLocation().clone();
    }

    @Override
    public void setLocation(PointF location) {
        subMover.setLocation(location);
    }

    @Override
    public MoveDirection getDirection() {
        return subMover.getDirection();
    }
}
