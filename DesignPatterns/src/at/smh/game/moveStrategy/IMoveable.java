package at.smh.game.moveStrategy;

import at.smh.game.PointF;
import org.newdawn.slick.GameContainer;

public interface IMoveable {
    void update(GameContainer gameContainer, int i);

    void setLocation(PointF location);
    PointF getLocation();
    MoveDirection getDirection();
}
