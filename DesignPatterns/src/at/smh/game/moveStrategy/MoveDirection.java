package at.smh.game.moveStrategy;

public enum MoveDirection {
    Upwards,
    Downwards,
    Right,
    Left
}
