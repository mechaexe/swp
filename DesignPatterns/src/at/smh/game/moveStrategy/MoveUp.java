package at.smh.game.moveStrategy;

import at.smh.game.GlobalRenderSettings;
import at.smh.game.PointF;
import org.newdawn.slick.GameContainer;

public class MoveUp implements IMoveable {
    private PointF location;

    public MoveUp(PointF initLoc){
        location = initLoc.clone();
    }

    @Override
    public void update(GameContainer gameContainer, int i) {
        location.setY(location.getY() - 1 * i * GlobalRenderSettings.getInstance().getSpeedMultiplier());
    }

    @Override
    public PointF getLocation() {
        return location.clone();
    }

    @Override
    public void setLocation(PointF location) {
        this.location = location.clone();
    }

    @Override
    public MoveDirection getDirection() {
        return MoveDirection.Upwards;
    }
}
