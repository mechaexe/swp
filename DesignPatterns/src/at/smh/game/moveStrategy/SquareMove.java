package at.smh.game.moveStrategy;

import at.smh.game.PointF;
import org.newdawn.slick.GameContainer;

import java.awt.geom.Dimension2D;

public class SquareMove implements IMoveable {
    private IMoveable subMover;
    private Dimension2D rectSize;

    public SquareMove(IMoveable initMover, Dimension2D rectSize){
        subMover = initMover;
        this.rectSize = rectSize;
    }

    @Override
    public void update(GameContainer gameContainer, int i) {
        PointF loc = getLocation();
        if(loc.getX() <= 0){
            loc.setX(1);
            subMover = new MoveUp(loc);
        }

        if(loc.getY() <= 0){
            loc.setY(1);
            subMover = new MoveRight(loc);
        }

        if(loc.getX() > gameContainer.getWidth() - rectSize.getWidth()){
            loc.setX(loc.getX()-1);
            subMover = new MoveDown(loc);
        }

        if(loc.getY() > gameContainer.getHeight() - rectSize.getHeight()){
            loc.setY(loc.getY()-1);
            subMover = new MoveLeft(loc);
        }

        subMover.update(gameContainer, i);
    }

    @Override
    public PointF getLocation() {
        return subMover.getLocation().clone();
    }

    @Override
    public void setLocation(PointF location) {
        subMover.setLocation(location);
    }

    @Override
    public MoveDirection getDirection() {
        return subMover.getDirection();
    }
}
