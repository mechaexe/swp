package at.smh.game.actors;

import at.smh.game.PointF;
import at.smh.game.moveStrategy.MoveDirection;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import java.awt.geom.Dimension2D;

public class LegacyActor {

    private PointF location;
    private Shape shape;

    public LegacyActor(PointF startLoc, Dimension2D size){
        location = startLoc;
        this.shape = new Rectangle(location.getX(), location.getY(), (float)size.getWidth(), (float)size.getHeight());
    }

    public void draw(GameContainer gameContainer, Graphics graphics){
        shape.setLocation(location.getX(), location.getY());
        graphics.draw(shape);
    }

    public void move(GameContainer gameContainer, int i){
        if(gameContainer.getWidth() < location.getX())
            location.setX(-shape.getWidth());
        location.setX(location.getX()+1);
    }

    public void onHittingActor(LegacyActor target){
    }

    public MoveDirection getMoveDirection(){
        return MoveDirection.Upwards;
    }

    public void move(PointF location){
        this.location = location.clone();
    }

    public PointF getLocation(){
        return location;
    }

    public void setShape(Shape s){
        this.shape = s;
    }

    public Shape getShape() {
        return shape;
    }
}
