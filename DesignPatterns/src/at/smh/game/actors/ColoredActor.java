package at.smh.game.actors;

import at.smh.game.PointF;
import at.smh.game.moveStrategy.MoveDirection;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.geom.Shape;

public class ColoredActor implements IActor{
    protected IActor actor;
    private ShapeFill shapeFill;

    public ColoredActor(IActor actor, ShapeFill shapeFill){
        this.actor = actor;
        this.shapeFill = shapeFill;
    }

    @Override
    public Shape getShape() {
        return actor.getShape();
    }

    @Override
    public void onIntersects(IActor host, IActor target) {
        actor.onIntersects(host, target);
    }

    @Override
    public void update(GameContainer gameContainer, int i) {
        actor.update(gameContainer, i);
    }

    @Override
    public void setLocation(PointF location) {
        actor.setLocation(location);
    }

    @Override
    public PointF getLocation() {
        return actor.getLocation();
    }

    @Override
    public MoveDirection getDirection() {
        return actor.getDirection();
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) {
        graphics.fill(actor.getShape(), shapeFill);
    }
}
