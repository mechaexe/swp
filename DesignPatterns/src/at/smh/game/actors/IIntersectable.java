package at.smh.game.actors;

public interface IIntersectable {
    void onIntersects(IActor host, IActor target);
}
