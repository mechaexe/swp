package at.smh.game.actors;

import at.smh.game.PointF;
import at.smh.game.moveStrategy.IMoveable;
import at.smh.game.moveStrategy.MoveDirection;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

public abstract class AbstractActor implements IActor {

    protected IMoveable mover;
    private Shape shape;

    public AbstractActor(Shape shape, IMoveable mover){
        this.mover = mover;
        this.shape = shape;
    }

    @Override
    public Shape getShape(){
        PointF loc = mover.getLocation();
        shape.setLocation(loc.getX(), loc.getY());
        return shape;
    }

    @Override
    public void setLocation(PointF location) {
        mover.setLocation(location);
    }

    @Override
    public PointF getLocation(){
        return mover.getLocation();
    }

    @Override
    public MoveDirection getDirection() {
        return mover.getDirection();
    }

    @Override
    public void update(GameContainer gameContainer, int i){
        mover.update(gameContainer, i);
    }

    public void render(GameContainer gameContainer, Graphics graphics){
        graphics.draw(getShape());
    }
}
