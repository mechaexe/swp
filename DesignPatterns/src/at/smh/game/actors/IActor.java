package at.smh.game.actors;

import at.smh.game.renderer.IRenderable;
import at.smh.game.moveStrategy.IMoveable;
import org.newdawn.slick.geom.Shape;

public interface IActor extends IRenderable, IMoveable, IIntersectable {
    Shape getShape();
}
