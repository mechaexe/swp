package at.smh.game.actors;

import at.smh.game.PointF;
import at.smh.game.moveStrategy.MoveRight;
import at.smh.game.moveStrategy.SquareMove;
import org.newdawn.slick.geom.Rectangle;
import java.awt.geom.Dimension2D;

public class RectActor extends AbstractActor {

    public RectActor(Dimension2D size) {
        super(new Rectangle(0,0,(int)size.getWidth(),(int)size.getHeight()), new SquareMove(new MoveRight(new PointF(1,1)), size));
    }

    public void onIntersects(IActor host, IActor targetActor) {
    }
}
