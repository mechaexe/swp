package at.smh.game.actors;

import at.smh.game.PointF;
import at.smh.game.moveStrategy.WarpMove;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;

import java.awt.*;

public class CircleActor extends AbstractActor {

    private int radius;

    public CircleActor(int radius){
        super(new Circle(0,0,radius), new WarpMove(new Dimension(radius*2,radius*2)));
        this.radius = radius;
    }

    public void onIntersects(IActor host, IActor targetActor) {
        mover.setLocation(new PointF(50, -radius*2));
    }
}
