package at.smh.game.actors;

import at.smh.game.moveStrategy.IMoveable;
import org.newdawn.slick.geom.Shape;

public class CustomActor extends AbstractActor {

    private IIntersectable intersectAction;

    public CustomActor(Shape shape, IMoveable mover, IIntersectable onIntersectAction) {
        this(shape, mover);
        intersectAction = onIntersectAction;
    }

    public CustomActor(Shape shape, IMoveable mover) {
        super(shape, mover);
    }

    @Override
    public void onIntersects(IActor host, IActor target) {
        if(intersectAction != null)
            intersectAction.onIntersects(host, target);
    }
}
