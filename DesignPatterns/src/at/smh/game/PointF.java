package at.smh.game;

public class PointF {
    private float X, Y;

    public PointF(){
        X = 0;
        Y = 0;
    }
    public PointF(float X, float Y){
        this.X = X;
        this.Y = Y;
    }

    public void setX(float x) {
        X = x;
    }

    public void setY(float y) {
        Y = y;
    }

    public float getX(){
        return X;
    }

    public float getY() {
        return Y;
    }

    public PointF clone(){
        return new PointF(X, Y);
    }

    public String toString(){
        return "PointF: [x]: " + X + " [y]: " + Y;
    }
}
