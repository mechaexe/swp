package at.smh.game;

import at.smh.game.actors.IActor;
import at.smh.game.actors.LegacyActor;
import at.smh.game.adapters.LegacyAdapter;
import at.smh.game.factories.ColoredFactory;
import at.smh.game.factories.IActorFactory;
import at.smh.game.factories.RandomActorFactory;
import org.newdawn.slick.*;
import org.newdawn.slick.Graphics;

import java.awt.*;
import java.util.List;

public class Main extends BasicGame {

    private List<IActor> actors;

    public Main() {
        super("Warmuptask");
    }

    @Override
    public void init(GameContainer gameContainer) throws SlickException {
        IActorFactory actorFactory = new RandomActorFactory();
        LegacyActor legacyActor = new LegacyActor(new PointF(10, 50), new Dimension(20,30));
        actorFactory = new ColoredFactory(actorFactory);

        actors = actorFactory.generateActors(5);
        actors.add(new LegacyAdapter(legacyActor));

        GlobalRenderSettings.getInstance().setSpeedMultiplier(2f);
    }

    @Override
    public void update(GameContainer gameContainer, int i) throws SlickException {
        for(IActor a : actors){
            a.update(gameContainer, i);
            notifyOnIntersect(a);
        }
    }

    private void notifyOnIntersect(IActor target){
        for(IActor b : actors){
            if(b.getShape().intersects(target.getShape()) && target != b){
                target.onIntersects(target, b);
                b.onIntersects(b, target);
            }
        }
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
        for(IActor a : actors)
            a.render(gameContainer, graphics);
    }

    public static void main(String[] argv) {

        try {
            AppGameContainer container = new AppGameContainer(new Main());
            container.setDisplayMode(1280, 720, false);
            container.setTargetFrameRate(60);
            container.start();
        } catch (SlickException var2) {
            var2.printStackTrace();
        }
    }
}
