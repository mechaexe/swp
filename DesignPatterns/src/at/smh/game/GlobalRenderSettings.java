package at.smh.game;

public class GlobalRenderSettings {
    private static GlobalRenderSettings _instance = new GlobalRenderSettings();

    public static GlobalRenderSettings getInstance() {
        return _instance;
    }

    private float speedMultiplier = 1;

    private GlobalRenderSettings() {
    }

    public float getSpeedMultiplier() { return speedMultiplier; }

    public void setSpeedMultiplier(float newSpeedMultiplier) { speedMultiplier = newSpeedMultiplier; }
}
