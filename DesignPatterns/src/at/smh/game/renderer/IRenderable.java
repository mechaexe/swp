package at.smh.game.renderer;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public interface IRenderable {
    void render(GameContainer gameContainer, Graphics graphics);
}
