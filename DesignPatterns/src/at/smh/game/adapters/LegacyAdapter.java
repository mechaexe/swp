package at.smh.game.adapters;

import at.smh.game.PointF;
import at.smh.game.actors.IActor;
import at.smh.game.actors.LegacyActor;
import at.smh.game.moveStrategy.MoveDirection;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;

import java.awt.*;

public class LegacyAdapter implements IActor {

    protected LegacyActor actor;

    public LegacyAdapter(LegacyActor actor){
        this.actor = actor;
    }

    @Override
    public Shape getShape() {
        return actor.getShape();
    }

    @Override
    public void onIntersects(IActor host, IActor target) {
        actor.onHittingActor(parseActor(target));
    }

    @Override
    public void update(GameContainer gameContainer, int i) {
        actor.move(gameContainer, i);
    }

    @Override
    public void setLocation(PointF location) {
        actor.move(location);
    }

    @Override
    public PointF getLocation() {
        return actor.getLocation();
    }

    @Override
    public MoveDirection getDirection() {
        return actor.getMoveDirection();
    }

    @Override
    public void render(GameContainer gameContainer, Graphics graphics) {
        actor.draw(gameContainer, graphics);
    }

    private static LegacyActor parseActor(IActor actor){
        LegacyActor res = new LegacyActor(actor.getLocation(), new Dimension(0,0));
        res.setShape(actor.getShape());
        return res;
    }
}
