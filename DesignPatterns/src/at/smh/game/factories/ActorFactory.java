package at.smh.game.factories;

import at.smh.game.actors.IActor;

import java.util.ArrayList;
import java.util.List;

public abstract class ActorFactory implements IActorFactory{

    @Override
    public List<IActor> generateActors(int count) {
        ArrayList<IActor> res = new ArrayList<>(count);
        for(int i = 0; i<count; i++)
            res.add(getActor());

        return res;
    }

    protected abstract IActor getActor();
}
