package at.smh.game.factories;

import at.smh.game.actors.ColoredActor;
import at.smh.game.actors.IActor;
import org.newdawn.slick.Color;
import org.newdawn.slick.ShapeFill;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ColoredFactory implements IActorFactory {
    protected IActorFactory factory;
    private Random rnd;

    public ColoredFactory(IActorFactory factory){
        this.factory = factory;
        rnd = new Random(new Date().getTime());
    }


    @Override
    public List<IActor> generateActors(int count) {

        List<IActor> actors = factory.generateActors(count);
        List<IActor> res = new ArrayList<>();
        for(IActor a : actors){
//            Color clr = ;
            res.add(new ColoredActor(a, new ShapeFill() {
                @Override
                public Color colorAt(Shape shape, float v, float v1) {
                    return new Color(rnd.nextInt(255),rnd.nextInt(255),rnd.nextInt(255));
                }

                @Override
                public Vector2f getOffsetAt(Shape shape, float v, float v1) {
                    return new Vector2f();
                }
            }));
        }
        return res;
    }
}
