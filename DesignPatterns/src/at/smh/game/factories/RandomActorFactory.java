package at.smh.game.factories;

import at.smh.game.PointF;
import at.smh.game.actors.CustomActor;
import at.smh.game.actors.IActor;
import at.smh.game.moveStrategy.IMoveable;
import at.smh.game.moveStrategy.MoveRight;
import at.smh.game.moveStrategy.SquareMove;
import at.smh.game.moveStrategy.WarpMove;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

import java.awt.*;
import java.awt.geom.Dimension2D;
import java.util.Date;
import java.util.Random;

public class RandomActorFactory extends ActorFactory {
    private Random rnd;

    public RandomActorFactory(){
        rnd = new Random(new Date().getTime());
    }

    private IMoveable getRandomMoveStrategy(PointF initLoc, Dimension2D dimensions){
        long rndStr = rnd.nextInt();
        return rndStr % 2 == 0 ? new SquareMove(new MoveRight(initLoc), dimensions) : new WarpMove(dimensions);
    }

    private Shape getRandomShape(Dimension2D dimensions){
        long rndStr = rnd.nextInt();
        return rndStr % 2 == 0 ? new Rectangle(0,0, (float)dimensions.getWidth(), (float)dimensions.getHeight()) : new Circle(0,0,(float) dimensions.getHeight() / 2);
    }

    public IActor getActor(){
        PointF initLoc = new PointF(rnd.nextInt(500), rnd.nextInt(500));
        Dimension2D dims = new Dimension(rnd.nextInt(100), rnd.nextInt(100));
        Shape s = getRandomShape(dims);

        if(s instanceof Circle){
            ((Dimension) dims).setSize(dims.getHeight(),dims.getHeight());
        }

        IMoveable mv = getRandomMoveStrategy(initLoc, dims);

        return new CustomActor(s, mv);
    }
}
