package at.smh.game.factories;
import at.smh.game.actors.IActor;
import java.util.List;

public interface IActorFactory {
    List<IActor> generateActors(int count);
}
